HANGMAN_PHOTOS = {
    1 : "x-------x",
    2 : """x-------x
    |
    |
    |
    |
    |""",
    3 : """x-------x
    |       |
    |       0
    |
    |
    |""",
    4 : """x-------x
    |       |
    |       0
    |       |
    |
    |
    """,
    5 : """x-------x
    |       |
    |       0
    |      /|\\
    |
    |
    """,
    6 : """x-------x
    |       |
    |       0
    |      /|\\
    |      /
    |
    """,
    7 : """x-------x
    |       |
    |       0
    |      /|\\
    |      / \\
    |
    """}
MAX_TRIES = 6

def print_open_screen():
    """
    printing the amazing opening hangman logo
    """
    print("""  _    _                                         
 | |  | |                                        
 | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
 |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
 | |  | | (_| | | | | (_| | | | | | | (_| | | | |
 |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                      __/ |                      
                     |___/""")


def choose_word(file_path, index):
    """
    func will choose the secret word
    :param file_path: path to the file with the words
    :param index: the index of the word in the list
    :type index: int
    :return ret_tup: a tuple with amount of duplicates and the place of the secret word
    :rtype: tuple
    """
    ret_tup = () #the tuple that the func returns
    secret_word = "" #the two variables that will fill the tuple

    file_txt = open(file_path, "r")
    x = file_txt.read()
    word_list = x.split(" ")
    length = len(word_list) - 1
    index -= 1
    while index > length:
        index -= length + 1


    amount_of_dupli = len(set(word_list))

    ret_tup = (amount_of_dupli, word_list[index])
    return ret_tup


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    function gets a list of old tries, and a new try.
    function will check if the new guess is already exist in the old tries
    :param letter_guessed: a new guess.
    :param old_letters_guessed: list of old tries
    :type letter_guessed: string
    :type old_letters_guessed: list
    """
    return_value = True

    length = len(letter_guessed)
    low_guess = letter_guessed.lower()
    is_alphabet = low_guess.isalpha()

    if length >= 2:
        return_value = False
    elif not is_alphabet:
        return_value = False
    elif low_guess in old_letters_guessed:
        return_value = False
    else:
        return_value = True
    
    return return_value


def check_win(secret_word, old_letters_guessed):
    """
    func checks if the user won the game by guessing all the letters
    :param secret_word: the secret word needed to be guessed
    :type: string
    :param: old_letters_guessed: the letter that the user already guessed
    :type: list
    :return: true or false - if he won or not
    :rtype: boolean
    """
    return_str = ""
    length = len(secret_word)
    for i in range(length):
        is_it_true = False
        for j in range(len(old_letters_guessed)):
            if secret_word[i] == old_letters_guessed[j]:
                is_it_true = True
        if(is_it_true):
            return_str += secret_word[i]
        else:
            return_str += "_"
        if i + 1 < length:
            return_str += " "

    ret = "_" in return_str
    if ret == False:
        return True
    else:
       return False 


def try_update_letter_guessed(letter_guessed, old_letters_guessed):

    """
    function gets a guess and will add it to the list of old tries
    if the guess is invalid - it will print that its invalid
    :param letter_guessed: guess
    :param old_letters_guessed: list of old tries
    :type letter_guessed: string
    :type old_letters_guessed: list
    return: updated list of old tries
    rtype: list
    """
    is_guess_valid = check_valid_input(letter_guessed, old_letters_guessed) #bool - if its valid or not(what the func returned)


    if is_guess_valid == True: #if the guess is valid
        old_letters_guessed.append(letter_guessed)
        return True
    else: #if the guess is invalid
        print("X\n" + " -> ".join(sorted(old_letters_guessed)))
        return False


def show_hidden_word(secret_word, old_letters_guessed):
    """
    this function will show the hidden word
    :param secret_word: the secret word dah
    :type: string
    :param: old_letters_guessed: all the letter the user guessed
    :type: list
    :return return_str: the hidden word with all what the user guessed
    :rtype: string
    """
    return_str = ""
    length = len(secret_word)
    for i in range(length):
        is_it_true = False
        for j in range(len(old_letters_guessed)):
            if secret_word[i] == old_letters_guessed[j]:
                is_it_true = True
        if(is_it_true):
            return_str += secret_word[i]
        else:
            return_str += "_"
        if i + 1 < length:
            return_str += " "


    return return_str


def print_hangman(num_of_tries):
    """
    this function will print the correct state according to the times that the user tried to guess
    :param num_of_tries: the times that the user tries to guess
    :type: int
    :return: None
    """
    print(HANGMAN_PHOTOS[num_of_tries])





def main():
    old_letters_guessed = []
    num_of_tries = 1

    print_open_screen()

    file_path = str(input("Enter file path: "))
    index = int(input("Enter index: "))

    secret_word_tup = choose_word(file_path, index)
    secret_word = secret_word_tup[1]

    print(show_hidden_word(secret_word, old_letters_guessed))
    while check_win(secret_word, old_letters_guessed) != True and num_of_tries <= MAX_TRIES: #a turn loop
        letter_guessed = str(input("Guess a letter: "))
        is_it_valid = check_valid_input(letter_guessed, old_letters_guessed) #got a guess letter and check if its valid!
        if is_it_valid == True:
            old_letters_guessed.append(letter_guessed)
            if letter_guessed in secret_word:
                try_update_letter_guessed(letter_guessed, old_letters_guessed)
                print(show_hidden_word(secret_word, old_letters_guessed))
                
            else:
                print(":(")
                print_hangman(num_of_tries)
                num_of_tries += 1
        else:
            print("X")
        
        print(show_hidden_word(secret_word, old_letters_guessed))
        
    print(show_hidden_word(secret_word, old_letters_guessed))
    if check_win(secret_word, old_letters_guessed) == False:
        print("LOSE")
    elif num_of_tries <= MAX_TRIES:
        print("WIN")



if __name__ == "__main__":
    main()
