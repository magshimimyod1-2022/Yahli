import socket
from datetime import datetime
from datetime import timedelta

SERVER_IP = "34.218.16.79"
SERVER_PORT = 77
ASCII_DIFFERENCE = 96
AMOUNT_OF_DAYS_TO_CHECK = 4 #its 3, but the range of the loop is +1
ERROR_STR = "999"
NUMBER_OF_MSG = 3 #the slice we need to do to get the number 100/200/500... in the servers message
SERVER_RESPONSE = "200"
SERVER_ERROR_MSG = "500"
ADD_TO_INDEX = 6


def printMenu():
    """
    a function to print the menu of options
    """
    print("Welcome To WeatherClient!\n")
    print("choose an option: \n")
    print("1) Whats the weather today? ")
    print("2) Three days forward ")


def calcWeather(city_only, date):
    """
    function gets the full name of a city, and a date, and returns a tuple with
    the weather - (<temp in number>, <weather description>)

    :paprm city_name: name of city (str)
    :param date: date (str)
    :return: ret_tup: a tuple with the weather - (<temp in number>, <weather description>)
    """
    ret_tup = ()
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connecting to remote computer 77
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    #getting a messagr from the server
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    
    checksum = calc_checksum(city_only, date)

    #a message template - 100:REQUEST:city=<city>&date=<date>&checksum=<checksum>  
    msg = "100:REQUEST:city=" + city_only + "&date=" + date + "&checksum=" + checksum
    sock.sendall(msg.encode())
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    num_of_msg = server_msg[:NUMBER_OF_MSG] #got the number of msg (100/200/500...)
    
    if SERVER_RESPONSE == num_of_msg:
        #finding the range of indexes that contain the number of temperature.
        temp_first_indx = server_msg.find("&temp=") + ADD_TO_INDEX #we add 6, cuz len("&temp=") = 6, and find() finds the first index of what were looking for
        temp_last_indx = server_msg.find("&text=")
        #found the range of indexes that contain the number of temperature.
        temp_num = server_msg[temp_first_indx:temp_last_indx]

        weather_weather_description_first_indx = server_msg.find("&text=") + ADD_TO_INDEX
        weather_description = server_msg[weather_weather_description_first_indx:]#got the weather description

        ret_tup = (temp_num, weather_description)#a perfect tuple than contains all the weather info - (<temo in num><weather description>)
    elif SERVER_ERROR_MSG == num_of_msg:
        error_description = server_msg[NUMBER_OF_MSG:]
        ret_tup = (ERROR_STR, error_description) #999 and the error message that comes after.

    return ret_tup


def calc_checksum(city_and_country, date):
    """
    func will get city name, and date, and will calculate the checksum
    example of checksum calculation:
    checksum for eilat and 11.09.2018 = 47.22 (<sum of letters (a = 1, b = 2...)> . <sum of numbers of date>)
    """
    city_checksum = 0 #checksum before the dot. (city)
    date_checksum = 0 #checksum after the dot. (date)

    city_only = seperateCityFromCountry(city_and_country)
    
    for letter in city_only:
        city_checksum += ord(letter.lower()) - ASCII_DIFFERENCE #get the number of each letter and add it
    city_checksum = str(city_checksum)

    date = date.split("/")
    date = "".join(date)
    for digit in date: #a loop that will calculate the sum of the digits in the the date
        date_checksum += int(digit)
    date_checksum = str(date_checksum)

    checksum = city_checksum + "." + date_checksum

    return checksum

def threeDaysLater(city_only):
    """
    func will get a city, and will show the weather for three days after the current date
    """

    #a code to calculate dates today and three after (dd/mm/yyyy) 
    date_var = datetime.today().strftime(format='%d/%m/%Y')
    for i in range(AMOUNT_OF_DAYS_TO_CHECK):
        date = datetime.today() + timedelta(days=i)
        date_var = date.strftime(format='%d/%m/%Y')
        result = calcWeather(city_only, date_var)
        if (checkIfError(result)):
            print(result)
        else:
            print(date_var + " temp is: " + result[0] + "")
    # <credit> code from - https://docs.python.org/3/library/datetime.html

def seperateCityFromCountry(city_and_country):
    """
    func gets country and city, and returns city only
    """
    city_and_country = city_and_country.split(", ")
    return city_and_country[0]

def checkIfError(result):
    """
    func will check if theres an error and will return true or false
    """
    if result[0] == ERROR_STR:
        return True
    return False

def main():
    country_and_city = str(input("where do you live? "))
    printMenu()
    city_only = seperateCityFromCountry(country_and_city)
    choice = int(input("enter your choice: "))
    date = str(datetime.today().strftime(format='%d/%m/%Y'))
    if choice == 1:
        result = calcWeather(city_only, date)
        if (checkIfError(result)):
            print(result)
        else:
            print(date + ", Temperature: " + result[0] + ", " + result[1] + ".") #example: 24/04/2020, Temperature: 16.5, Few Clouds.
    elif choice == 2:
        threeDaysLater(city_only)
    else:
        print("invalid choice... choose 1 or 2")



if __name__ == "__main__":
    main()