import socket

OUR_SERVER_PORT = 42075
WELCOME_MESSAGE = "Welcome!\ntype 'menu' to see the menu, type 'exit' to disconnect"
MENU_MSG = 'menu'
UNRECOGNIZED_COMMAND_ERROR_MESSAGE = "We dont have this command, type 'menu' to see the menu"
HIGHEST_COMMAND_NUM = 8
RUNAWAY_CODE = 8 #a code to escape, exit, RUN AWAY from this trashy server
LOWEST_COMMAND_NUM = 1
INVALID_CHOICE_MSG = "we dont have that option, enter a number between 1 and 8"
INPUT_MSG = "choose from the menu (1 - 8): "
DIFF_CMMND_ASK_MSG = "we dont have this command, type 'menu' to see the menu"
QUIT_MSG = "exit"


def print_albums():
    """
    func returns a list of all the albums
    """
    
    """
    albums = []
    with open("albums.txt", "r") as albums_file:
        for line in albums_file:
            albums.append(line.strip()) #strip() removes any spaces at the beginning to end (credit w3schools!)
    return albums
    and now i realized that i dont need to write the functions now.... yay
    """
    return "albums: \n"

def print_songs_in_album():
    """
    TODO: implement this func
    """
    return "songs in album: \n"
def get_length_of_song():
    """
    TODO: implement this func
    """
    return "length of song: \n"                           
    
def get_lyrics():
    """
    TODO: implement this func
    """
    return "lyrics: \n"
def find_album_by_song():
    """
    TODO: implement this func
    """
    return "album by song: \n"
def find_song_by_name():
    """
    TODO: implement this func
    """
    return "song by name: \n"
def find_song_by_lyrics():
    """
    TODO: implement this func
    """
    return "song by lyrics: \n"
def exit_code():
    global client_msg #making it global so it will change outside the function
    client_msg = QUIT_MSG #making it quit
    
COMMAND_DICT = {
    1: print_albums(),
    2: print_songs_in_album(),
    3: get_length_of_song(),
    4: get_lyrics(),
    5: find_album_by_song(),
    6: find_song_by_name(),
    7: find_song_by_lyrics(),
    8: exit_code()
}   #a dict for all options    

def create_server_socket():
    """
    func creates a socket and listens to the server
    :return: the message that the server sent
    """
    with socket.socket() as listening_sock:
        listening_sock.bind(('',OUR_SERVER_PORT))
        listening_sock.listen(1)
        client_soc, client_address = listening_sock.accept() 
        with client_soc:
            client_soc.sendall(WELCOME_MESSAGE.encode()) #send the welcome message
            client_msg = client_soc.recv(1024) #getting 'menu' (hopefully)
            client_msg = client_msg.decode() 
            while(QUIT_MSG != client_msg):
                #thank you for not quiting from my amazing server!
                
                client_soc.sendall(INPUT_MSG.encode()) #ask for the input (which command he wants)
                choice = client_soc.recv(1024) #get the input command (1 - 8)
                choice = choice.decode()
                print(choice)
                if RUNAWAY_CODE == int(choice):
                    exit_code() #if the client wants to quit, we will make (QUIT_MSG != client_msg) not true and it will end
                client_soc.sendall(COMMAND_DICT[int(choice)].encode())#dictionary that holds the commands and their functions, we call and print what number choice function returns


        
    




def main():
    while True:
        create_server_socket()


if __name__ == "__main__":
    main()