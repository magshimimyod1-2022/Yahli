import socket

SERVER_IP = "127.0.0.1"
SERVER_PORT = 42075
MENU_MSG = "menu"
WELCOME_MESSAGE = "Welcome!\ntype 'menu' to see the menu, type 'exit' to disconnect"
UNRECOGNIZED_COMMAND_ERROR_MESSAGE = "We dont have this command, type 'menu' to see the menu"
HIGHEST_COMMAND_NUM = 8
LOWEST_COMMAND_NUM = 1
INVALID_CHOICE_MSG = "we dont have that option, enter a number between 1 and 8"
INPUT_MSG = "choose from the menu (1 - 8): "


def create_listening_socket():
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Connecting to my server
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    
    while(True):
        server_msg = sock.recv(1024)
        server_msg = server_msg.decode()
        print(server_msg) #getting the welcome message from my amazing, beautifull, totally best server ever! oh and also printing it
        
        option = str(input("please just type 'menu' cuz i dont have any other options for you: "))
        while MENU_MSG != option:
            option = input(UNRECOGNIZED_COMMAND_ERROR_MESSAGE)
        sock.sendall(str(option).encode())#str so we can use encode   sending him 'menu'
        print_menu()
        
        ask_for_input = sock.recv(1024) #get the ask for input
        ask_for_input = ask_for_input.decode()
        print(ask_for_input) #printing the server bakasha for input (command)
        
        choice = int(input()) #the server already sent input message
        while int(choice) > HIGHEST_COMMAND_NUM or int(choice) < LOWEST_COMMAND_NUM: #keep asking for correct input until the input is valid(1 - 8)
            print(INVALID_CHOICE_MSG)
            choice = input() #the server already sent input message
        sock.sendall(str(choice).encode())#sending him a valid command (1 - 8) str so we can use encode
        
        amazing_cmmnd = sock.recv(1024)
        amazing_cmmnd = amazing_cmmnd.decode()
        print(amazing_cmmnd)
        
        #ok, we will give you a chance to runaway
        exit_ques_mark = input("do you want to exit? type 'y' to leave: ")
        if exit_ques_mark == 'y':
            exit() #stop the program
        else: #ohhhh you wanna stayyyy thx!
            print("thank you for staying with us")
            print("again\n\n")   

def print_menu():
    """
    func prints the menu - could you believe?!
    """
    
    print("Choose from all these options: \ntype the number of the option you want to do:\n\n")
    print("1 - print albums\n")
    print("2 - print songs in album\n")
    print("3 - get length of song\n")
    print("4 - get lyrics\n")
    print("5 - find album by song\n")
    print("6 - find song by name\n")
    print("7 - find song by lyrics\n")
    print("8 - disconnect, exit\n")
    
def main():
    while True:
        create_listening_socket()
        
if __name__ == "__main__":
    main()
    