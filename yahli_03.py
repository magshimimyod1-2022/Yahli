import socket

SERVER_IP = "54.71.128.194"
SERVER_PORT = 92
OUR_SERVER_PORT = 9090
RACIST_ERROR_MESSAGE = 'ERROR#"France is banned!"' #i swear im not racist! ive been told to do it!
BLACK_LIST_PATH = "C:\\Users\\test0\OneDrive\שולחן העבודה\\magshimim\\coding networks\\blacklist.txt" #the path to the blacklist file
ADD_TO_INDEX = 8 #first use
ADD_TO_INDEX_2 = 6 #second use
BANNED_ACTOR_ERROR_MESSAGE = 'ERROR#"Sorry, one of the actors is banned from this client!'
REVERSED_YEARS_ERROR_MESSAGE = 'ERROR#"End year is less or equal to start year"'

def bannedActors(actors):
    """
    func will read the file and ban the actors that are in it
    getting actors from other func
    return: true if the actors are in the blacklist, false otherwise
    """
    
    src_txt = open(BLACK_LIST_PATH, "r")
    text = src_txt.read() #opening the file
    
    lines = text.split("\n")#split it with lines
    
    for line in range(len(lines)): #checking every actor
        if lines[line] in actors: #if we found a banned actor, we will send the error message to the client
            return True
    return False
            
            
    
    
def create_sending_socket(msg):
    """
    func creates a socket and sends the message to the server
    """
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    sock.sendall(msg.encode())
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    return server_msg


def create_listening_socket():
    """
    func creates a socket and listens to the server
    :return: the message that the server sent
    """
    with socket.socket() as listening_sock:
        listening_sock.bind(('',OUR_SERVER_PORT))
        listening_sock.listen(1)
        client_soc, client_address = listening_sock.accept() 
        with client_soc:
            client_msg = client_soc.recv(1024) 
            client_msg = client_msg.decode()
            print(client_msg)
            
            #getting years
            years_first_idx = client_msg.find("&year:") + ADD_TO_INDEX_2
            years_last_idx = client_msg.find("&country:")
            years = client_msg[years_first_idx:years_last_idx]
            years_list = years.split("-") #got a list[from year, to year]
            #got years
            
            if (int(years_list[0]) >= int(years_list[1])): #if the the years are reversed, we will send an error message
                print(REVERSED_YEARS_ERROR_MESSAGE)
                client_soc.sendall(REVERSED_YEARS_ERROR_MESSAGE.encode())
            
            
            if "France" in client_msg or "france" in client_msg:
                print(RACIST_ERROR_MESSAGE)
                client_soc.sendall(RACIST_ERROR_MESSAGE.encode())#sorry france... nothing personal
            
            else: 
                servers_message = create_sending_socket(client_msg) #send the message to the server
                
                splited_msg = servers_message.split('"')
                splited_msg[1] += " proxi" #add proxi to the name
                
                servers_message = '"'.join(splited_msg)
                servers_message = servers_message.replace("jpg", ".jpg") #added . before the jpg so the image will load
                

                actors_first_index = servers_message.find('actors:"') + ADD_TO_INDEX #we add 8, because we want to start from the actors:
                actors_last_index = servers_message.find(',"&rating')
                actors = servers_message[actors_first_index:actors_last_index] #found the actors
                if (bannedActors(actors)): #if the actors are in the blacklist, we will send the error message
                    print(BANNED_ACTOR_ERROR_MESSAGE)
                    client_soc.sendall(BANNED_ACTOR_ERROR_MESSAGE.encode())
                else: #if the actors are not in the blacklist, we will send the message to the client
                    client_soc.sendall(servers_message.encode()) #send the message to the client
                    print(servers_message)
            

def main():
    while True:
        create_listening_socket()
    
    
if __name__ == "__main__":
    main()


"""
i just wanted to say that i love this course this semester!
first time im feeling like im actually coding!
i also love that we can finally do something creative that we think about.
thats it.... bye
"""