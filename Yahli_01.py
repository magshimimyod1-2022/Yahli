import requests
from collections import Counter

URL = "http://webisfun.cyber.org.il/nahman/files/" #the url of the web
FILES_URL = "http://webisfun.cyber.org.il/nahman/files/" #the url of the files web
INDEX_WE_NEED = 99 #the number that we need to get
LAST_LINE_WE_DONT_NEED = 8 #we dont need the first 8 lines
START_END_LINES = 25 
LAST_END_LINES = 28 #we dont need the last three lines
MAX_SPLITS = 2 #split when >, maximum 2 times

def extract_password_from_site():
    """
    func will get the url of a web, and will take the 100 char from each file in the order
    """
    password = "" #the password that we are gonna return
    response = requests.get(URL) #http request
    html = response.text
    lines = html.split("\n")#split it with lines

    #delete all the lines that we dont need
    for i in range(LAST_LINE_WE_DONT_NEED):
        del lines[0]
    for j in range(START_END_LINES, LAST_END_LINES):
        lines.pop()
    #finish deleting all the lines that we dont need

    for line in range(len(lines)):
        #take every line, and split it to get only what we need
        file_number_to_add = str(lines[line]).split(">", MAX_SPLITS)#split when >, maximum 2 times
        file_number_to_add_two = str(file_number_to_add[2]).split("<")
        file_number_to_add_final = file_number_to_add_two[0]
        #got what we need(file<number>.nfo)

        url_to_open = FILES_URL + str(file_number_to_add_final) #add what we want to the original url so we can open the files
        res = requests.get(url_to_open)
        text = res.text
        password += text[INDEX_WE_NEED] #getting 100 char of each file text

    return password

def find_most_common_words(file_path, words_in_sentence):
    """
    function will get a file path, and amount of words to be in the sentence.
    function will read the file, and will return a sentence with the
    <words_in_sentence> words that appeare most times

    :param file_path: a path to a file with words
    :param words_in_sentence: amounts of words the user want in his sentence

    :return: a sentence that contains <words_in_sentence> words that appeare most times in the file
    """
    sentence_to_return = ""
    with open(file_path, "r") as word_file:
        html = word_file.read()
    words_list = html.split(" ")
    counter = Counter(words_list) #returns a list of tuples  [(<word><amount of times the word appeared>, ...)]
    most_common = counter.most_common(words_in_sentence) #getting the x most common words
    for i in range(len(most_common)):
        sentence_to_return += most_common[i][0] #from the tuple, we take index 0 because we need the word only
        sentence_to_return += " "
    return sentence_to_return

def main():
    """
    getting a choice and call the functions
    """
    choice = int(input("choose: 1) - find password | 2) - find sentence: "))
    if choice == 1:
        print(extract_password_from_site())
    if choice == 2:
        file_path = "words.txt"
        words_in_sentence = 6
        print(find_most_common_words(file_path, words_in_sentence))

if __name__ == "__main__":
    main()